﻿### {{var:name}}({{var:args}})

{{var:summary}}

{{include:declaration}}

{{include:params}}

{{func:returns}}