﻿{{include:indexHeader}}

# Max C# API documentation

This API documentation explains all ``public`` and ``protected`` methods of the components in Max C#.

## Table of Contents

{{prefab:fullTOC}}