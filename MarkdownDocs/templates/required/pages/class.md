﻿{{include:classHeader}}

# class {{var:name}}
## Summary
{{var:summary}}

{{include:declaration}}
{{include:namespace}}

{{include:constructors}}

{{include:properties}}

{{include:methods}}

{{include:operators}}