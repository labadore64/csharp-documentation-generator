﻿{{include:structHeader}}

# struct {{var:name}}

{{var:summary}}

{{include:declaration}}
{{include:namespace}}

{{include:constructors}}

{{include:properties}}

{{include:methods}}