﻿{{include:interfaceHeader}}

# interface {{var:name}}

{{var:summary}}

{{include:declaration}}
{{include:namespace}}

{{include:properties}}

{{include:methods}}