﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace MarkdownDocs.option
{
    public static class DocumentationOptions
    {
        /// <summary>
        /// Struct representing a single option.
        /// </summary>
        private struct OptionValue
        {
            public int[] Value;
            public string[] StringValue;

            public OptionValue(int Value)
            {
                this.Value = new int[] { Value };
                StringValue = new string[1];
            }

            public OptionValue(string StringValue)
            {
                Value = new int[1];
                this.StringValue = new string[] { StringValue };
            }

            public OptionValue(int Value, string StringValue)
            {
                this.Value = new int[] { Value };
                this.StringValue = new string[] { StringValue };
            }

            public OptionValue(int[] value)
            {
                this.Value = value;
                StringValue = new string[1];
            }

            public OptionValue(string[] value)
            {
                Value = new int[1];
                StringValue = value;
            }
            public OptionValue(int[] intvalue, string[] stringvalue)
            {
                Value = intvalue;
                StringValue = stringvalue;
            }
        }

        /// <summary>
        /// Options dictionary.
        /// </summary>
        static Dictionary<string, OptionValue> Options = new Dictionary<string, OptionValue>();

        /// <summary>
        /// Gets a value loaded at a specific option.
        /// </summary>
        /// <param name="Option">Option</param>
        /// <returns>Value</returns>
        public static int GetValue(string Option)
        {
            if (Options.ContainsKey(Option))
            {
                return Options[Option].Value[0];
            }

            return 0;
        }

        /// <summary>
        /// Gets a value loaded at a specific option.
        /// </summary>
        /// <param name="Option">Option</param>
        /// <returns>Value</returns>
        public static int[] GetValues(string Option)
        {
            if (Options.ContainsKey(Option))
            {
                return Options[Option].Value;
            }

            return new int[1];
        }

        /// <summary>
        /// Checks if a certain property exists.
        /// </summary>
        /// <param name="Option">Option</param>
        /// <returns>Value</returns>
        public static bool Contains(string Option)
        {
            return Options.ContainsKey(Option);
        }

        /// <summary>
        /// Gets a string stored at a specific option.
        /// </summary>
        /// <param name="Option">Option</param>
        /// <returns>String</returns>
        public static string GetString(string Option)
        {
            if (Options.ContainsKey(Option))
            {
                return Options[Option].StringValue[0];
            }

            return "";
        }

        /// <summary>
        /// Gets a string stored at a specific option.
        /// </summary>
        /// <param name="Option">Option</param>
        /// <returns>String</returns>
        public static string[] GetStrings(string Option)
        {
            if (Options.ContainsKey(Option))
            {
                return Options[Option].StringValue;
            }

            return new string[1];
        }

        /// <summary>
        /// Loads options from a json file.
        /// </summary>
        /// <param name="Filename">File</param>
        public static void LoadOptions(string Filename)
        {
            if (File.Exists(Filename))
            {
                JObject options = JObject.Parse(File.ReadAllText(Filename));
                if(options.Count > 0)
                {
                    DoOptions(options);
                }
            }

            Console.WriteLine("Options loaded.");
        } 

        private static void DoOptions(JToken options)
        {
            JToken[] objects = options.Children<JToken>().ToArray();
            if (objects.Length > 0)
            {
                for(int i = 0; i < objects.Length; i++)
                {
                    if (objects[i].Type == JTokenType.Array)
                    {
                        string path = options.Path;

                        JArray array = (JArray)objects[i];
                        string[] values = new string[array.Count];

                        for(int j = 0; j < values.Length; j++)
                        {
                            values[j] = array[j].ToString();
                        }

                        Options.Add(path, new OptionValue(values));
                    }
                    else
                    {
                        DoOptions(objects[i]);
                    }
                }
            } else
            {
                string path = options.Path;
                string value = options.ToString();

                int n;
                bool isNumeric = int.TryParse(value, out n);
                if (isNumeric)
                {
                    Options.Add(path, new OptionValue(n,value));
                } else
                {
                    Options.Add(path, new OptionValue(value));
                }
            }

        }
    }
}
