﻿using MarkdownDocs.machine;
using MarkdownDocs.option;
using System;

namespace MarkdownDocs
{
    class Program
    {
        static void Main(string[] args)
        {
            string options = "options.json";
            DocumentationOptions.LoadOptions(options);

            string directory = DocumentationOptions.GetString("global.source.directory");
            string FileType = ".cs";

            if (DocumentationOptions.Contains("global.source.ext"))
            {
                FileType = DocumentationOptions.GetString("global.source.ext");
            }

            DocGenerator Generator = new DocGenerator();
            Generator.DocGenerate(directory, FileType);

            Console.WriteLine("ok");
        }
    }
}
