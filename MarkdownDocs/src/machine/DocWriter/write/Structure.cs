﻿using MarkdownDocs.model;
using MarkdownDocs.option;
using System;
using System.IO;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private void WriteStructure(Block block, string structureType)
        {
            string contents = "";
            if (Templates.ContainsKey(structureType))
            {
                contents = Templates[structureType];
            }

            contents = ProcessVariables(contents, block.Variables, block);

            block.WriteFile(outDir, contents);
        }
    }
}
