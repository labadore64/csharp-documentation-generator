﻿using MarkdownDocs.model;
using System;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        /// <summary>
        /// Writes the block as a page.
        /// </summary>
        /// <param name="block"></param>
        public void WritePage(Block block)
        {
            Type blockType = block.GetType();
            
            if(blockType == typeof(IndexBlock))
            {
                WriteStructure(block,"index");
            } else if (blockType == typeof(NamespaceBlock))
            {
                WriteStructure(block, "namespaceIndex");
            } else
            {
                WriteStructure(block, block.StructureType);
            }
        }
    }
}
