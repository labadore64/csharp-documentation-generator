﻿using MarkdownDocs.model;
using MarkdownDocs.src.helper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string ProcessVariables(string String, Dictionary<string,string> vars, Block block)
        {
            string fullvar;
            List<string> variables = String.EverythingBetween("{{","}}");

            int invalidCounter ;
            string var;

            // do the work for include, var, prefab and func
            while (variables.Count > 0)
            {
                invalidCounter = 0;
                for (int i = 0; i < variables.Count; i++)
                {
                    fullvar = variables[i];
                    if (variables[i].Contains("include:"))
                    {
                        var = variables[i].Replace("include:", "");

                        // tests if you're trying to write certain things that could be empty.
                        // if they're empty they should be ignored
                        if (WriteProcessTestInclude(var,vars,block))
                        {
                            if (Templates.ContainsKey(var))
                            {
                                String = String.Replace("{{" + fullvar + "}}", Templates[var]);
                            }
                            else
                            {
                                invalidCounter++;
                            }
                        } else
                        {
                            String = String.Replace("{{" + fullvar + "}}", "");
                        }
                    } else if (variables[i].Contains("var:")){
                        var = variables[i].Replace("var:", "");
                        if (vars.ContainsKey(var))
                        {
                            String = String.Replace("{{" + fullvar + "}}", vars[var]);
                        }
                        else
                        {
                            invalidCounter++;
                        }
                    } else if (variables[i].Contains("prefab:"))
                    {
                        var = variables[i].Replace("prefab:", "");
                        if (Prefabs.ContainsKey(var))
                        {
                            String = String.Replace("{{" + fullvar + "}}", Prefabs[var]);
                        } else
                        {
                            invalidCounter++;
                        }
                    }
                    else if (variables[i].Contains("func:"))
                    {
                        var = variables[i].Replace("func:", "");
                        string result = WriteFuncProcess(var, block,block.Variables);
                        if(result.Length > 0)
                        {
                            String = String.Replace("{{" + fullvar + "}}", result);
                        } else
                        {
                            invalidCounter++;
                        }
                    }
                    else
                    {
                        invalidCounter++;
                    }
                }

                // if all the replacers are invalid
                if(invalidCounter == variables.Count)
                {
                    break;
                }

                variables = String.EverythingBetween("{{", "}}");
            }

            return String;
        }

        private bool WriteProcessTestInclude(string tester, Dictionary<string, string> vars, Block block)
        {
            // check if has methods
            if(tester == "methods")
            {
                if(block.Components.Count(x=>x.Type == "method") == 0)
                {
                    return false;
                }
            }
            // check if has operators
            if (tester == "operators")
            {
                if (block.Components.Count(x => x.Type == "operator") == 0)
                {
                    return false;
                }
            }
            // check if has fields
            if (tester == "fields")
            {
                if (block.Components.Count(x => x.Type == "field") == 0)
                {
                    return false;
                }
            }
            // check if has properties
            if (tester == "properties")
            {
                if (block.Components.Count(x => x.Type == "property") == 0)
                {
                    return false;
                }
            }
            // check if has constructor
            if (tester == "constructors")
            {
                if (block.Components.Count(x => x.Type == "constructor") == 0)
                {
                    return false;
                }
            }
            // check if has implements
            if (tester == "implements")
            {
                if (block.Implement.Length == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
