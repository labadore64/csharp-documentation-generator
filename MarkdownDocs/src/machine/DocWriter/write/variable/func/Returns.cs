﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncReturns(Block block, Dictionary<string, string> vars)
        {
            string Returner = " ";

            string template = Templates["returns"];

            if (vars.ContainsKey("returns"))
            {
                Returner += ProcessVariables(template, vars, block);
            }


            return Returner;
        }
    }
}
