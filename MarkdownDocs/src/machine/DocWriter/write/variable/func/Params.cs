﻿using MarkdownDocs.model;
using System;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncParams(Block block, Dictionary<string, string> vars, string[] par)
        {
            string Returner = "";

            string template = Templates["param"];

            int counter = 0;

            Dictionary<string, string> newVars;

            while (vars.ContainsKey("paramname" + counter))
            {
                newVars = new Dictionary<string, string>();
                newVars.Add("paramtype", vars["paramtype" + counter]);
                newVars.Add("paramname", vars["paramname" + counter]);
                newVars.Add("paramdesc", vars["paramdesc" + counter]);
                Returner += ProcessVariables(template, newVars, block);

                counter++;
            }

            return Returner;
        }
    }
}
