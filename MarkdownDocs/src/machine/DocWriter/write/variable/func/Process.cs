﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncProcess(string variable, Block block, Dictionary<string,string> vars)
        {
            string returner = "";
            if(variable == "TOCChildren")
            {
                returner = WriteFuncTOCChildren(block);
            } else if (variable == "method"
                                || variable == "constructor"
                                || variable == "property"
                                || variable == "operator")
                    {
                returner = WriteFuncChildVariable(block, variable);
            }
            else if (variable == "namespace")
            {
                returner = WriteFuncNamespace(block);
            }

            return returner;
        }
    }
}
