﻿using MarkdownDocs.model;
using System;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncTOCChildren(Block block)
        {
            string Returner = "";
            List<Block> children = block.Children.FindAll(x => x.StructureType == "namespace");

            for(int i = 0; i < children.Count; i++)
            {
                Returner += "* " + children[i].MDLink + Environment.NewLine;
            }

            children = block.Children.FindAll(x => x.StructureType == "class" ||
                                                   x.StructureType == "enum" ||
                                                   x.StructureType == "struct" ||
                                                   x.StructureType == "interface");

            for (int i = 0; i < children.Count; i++)
            {
                Returner += "* **" + children[i].MDLink + "** - " + children[i].GetVar("summary") + Environment.NewLine;
            }

            return Returner;
        }
    }
}
