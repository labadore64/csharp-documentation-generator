﻿using MarkdownDocs.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncChildVariable(Block block, string type)
        {
            string Returner = "";
            List<BlockComponent> children = block.Components.FindAll(x => x.Type == type);
            string MethodTemplate = Templates[type];
            string thisTemplate;
            Dictionary<string, string> vars;
            string holder;
            for (int i = 0; i < children.Count; i++)
            {
                thisTemplate = MethodTemplate;
                vars = new Dictionary<string, string>(children[i].Comments);
                vars.Add("name", children[i].TypeName);
                vars.Add("declaration", children[i].Statement);
                string paramsa = "";
                for (int j = 0; j < children[i].Params.Length; j++)
                {
                    if (!Regex.Match(children[i].Params[j], @"\[[^>]+\]|&nbsp;").Success)
                    {
                        if (children[i].Params[j].Length > 0)
                        {
                            paramsa += children[i].Params[j] + ", ";
                        }
                    }
                }
                if (paramsa.Length < 2)
                {
                    vars.Add("args", "");
                }
                else
                {
                    vars.Add("args", paramsa.Substring(0, paramsa.Length - 2));
                }

                if (vars.Count(x => x.Key.Contains("param") && !x.Key.Contains("typeparam")) == 0)
                {
                    thisTemplate = thisTemplate.Replace("{{func:params}}", "").Replace("{{include:params}}", "");
                }
                if (vars.Count(x => x.Key.Contains("returns")) == 0)
                {
                    thisTemplate = thisTemplate.Replace("{{func:returns}}", "").Replace("{{include:returns}}", "");
                }

                holder = ProcessVariables(thisTemplate, vars, block) + Environment.NewLine;

                // do params

                holder = holder.Replace("{{func:params}}", WriteFuncParams(block, vars, children[i].Params));
                holder = holder.Replace("{{func:returns}}", WriteFuncReturns(block, vars));
                // do returns

                Returner += holder;
            }

            if (Returner == "")
            {
                Returner = " ";
            }

            return Returner;
        }
    }
}
