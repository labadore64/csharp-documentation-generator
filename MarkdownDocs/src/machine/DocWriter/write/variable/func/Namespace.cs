﻿using MarkdownDocs.model;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private string WriteFuncNamespace(Block block)
        {
            string Returner = "";

            if (DocGenerator.BlockDictionary.ContainsKey(block.Namespace))
            {
                Returner = "<a href=\""+DocGenerator.BlockDictionary[block.Namespace].URLLink+"\">" + block.Namespace + "</a>";
            }

            return Returner;
        }
    }
}
