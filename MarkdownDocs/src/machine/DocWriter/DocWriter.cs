﻿using MarkdownDocs.option;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        string outDir = "";
        public DocWriter()
        {
            if (DocumentationOptions.Contains("global.out.directory"))
            {
                outDir = DocumentationOptions.GetString("global.out.directory");
                Initialize();
            }
        }
    }
}
