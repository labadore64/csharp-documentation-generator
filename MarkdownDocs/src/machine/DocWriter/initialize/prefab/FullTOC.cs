﻿using MarkdownDocs.model;
using MarkdownDocs.option;
using System;
using System.Collections.Generic;
using System.Text;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private void PrefabInitFullTOC()
        {
            int Counter = 0;
            stringBuilder.Clear();

            Block root = DocGenerator.RootBlock;

            if(root != null)
            {
                for (int i = 0; i < root.Children.Count; i++)
                {
                    PrefabInitFullTOC_DoBlock(root.Children[i], Counter);
                }
            }

            Prefabs.Add("fullTOC", stringBuilder.ToString());

            stringBuilder.Clear();
        }

        private void PrefabInitFullTOC_DoBlock(Block block, int Counter)
        {
            for (int i = 0; i < Counter; i++)
            {
                stringBuilder.Append("\t");
            }

            Counter++;

            stringBuilder.Append("* ");
            stringBuilder.Append(block.MDLink);
            stringBuilder.Append(Environment.NewLine);
            for(int i = 0; i < block.Children.Count; i++){
                PrefabInitFullTOC_DoBlock(block.Children[i],Counter);    
            }
            Counter--;
        }
    }
}
