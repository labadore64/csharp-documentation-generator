﻿using MarkdownDocs.model;
using MarkdownDocs.option;
using System.IO;

namespace MarkdownDocs.machine
{
    internal partial class DocWriter
    {
        private void LoadTemplates()
        {
            string templatesDir = "templates\\";

            if (DocumentationOptions.Contains("global.template.directory"))
            {
                templatesDir = DocumentationOptions.GetString("global.template.directory");
            }

            foreach (string file in Directory.GetFiles(
                templatesDir, "*", SearchOption.AllDirectories))
            {
                Templates.Add(Path.GetFileNameWithoutExtension(file), File.ReadAllText(file));
            }
        }
    }
}
