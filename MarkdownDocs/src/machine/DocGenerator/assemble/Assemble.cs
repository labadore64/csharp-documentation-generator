﻿namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private void Assemble()
        {
            // Assemble the types from assembling the namespaces.
            AssembleTypes(AssembleNamespaces());

            // assemble the list
            AssembleBlockList();

        }

    }
}
