﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private void AssembleTypes(string[][] namespacesSorted)
        {

            // create namespace blocks for every namespace
            List<NamespaceBlock> NameSpaceBlocks = new List<NamespaceBlock>();
            NamespaceBlock holder;

            for (int i = 0; i < Namespaces.Count; i++)
            {
                holder = new NamespaceBlock();
                holder.Namespace = Namespaces[i];

                NameSpaceBlocks.Add(holder);
            }

            // attach the other blocks to the namespace blocks
            NamespaceBlock hold;
            foreach (KeyValuePair<string, TypeBlock> block in Blocks)
            {
                hold = NameSpaceBlocks.Find(x => x.Namespace == block.Key.Substring(0, block.Key.LastIndexOf('.')));
                if (hold != null)
                {
                    hold.AddChild(block.Value);
                }
            }

            // do the parenting of the namespace blocks

            string[] currentArrays;
            string[] lowerLevelArrays;
            // note this one goes in reverse
            for (int i = namespacesSorted.Length - 1; i >= 1; i--)
            {
                currentArrays = namespacesSorted[i];
                lowerLevelArrays = namespacesSorted[i - 1];

                for (int j = 0; j < currentArrays.Length; j++)
                {
                    for (int k = 0; k < lowerLevelArrays.Length; k++)
                    {
                        if (currentArrays[j].Contains(lowerLevelArrays[k]))
                        {
                            NameSpaceBlocks.Find(x => x.Namespace == lowerLevelArrays[k]).AddChild(
                                NameSpaceBlocks.Find(x => x.Namespace == currentArrays[j]));
                        }
                    }
                }
            }

            // for the root, create the index and put everything in the top level as its child.
            RootBlock = new IndexBlock();

            currentArrays = namespacesSorted[0];
            for(int i = 0; i < currentArrays.Length; i++)
            {
                RootBlock.AddChild(NameSpaceBlocks.Find(x => x.Namespace == currentArrays[i]));
            }
        }
    }
}
