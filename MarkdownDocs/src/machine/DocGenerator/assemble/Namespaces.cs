﻿using System.Linq;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private string[][] AssembleNamespaces()
        {
            GetAllNames();
            
            // sort the names
            Namespaces = Namespaces.OrderBy(o => o.Count(x => x == '.')).ToList();

            // separate names by quantity

            string[][] namespaceHolder = new string[Namespaces[Namespaces.Count - 1].Count(x => x == '.')+1][];

            for(int i = 0; i < namespaceHolder.Length; i++)
            {
                namespaceHolder[i] = Namespaces.FindAll(x => x.Count(x => x == '.') == (i)).ToArray();
            }

            return namespaceHolder;
        }

        private void GetAllNames()
        {
            string[] splitSpaces;
            string[] namespacesCopy = Namespaces.ToArray();

            for (int k = 0; k < namespacesCopy.Length; k++)
            {
                _stringbuilder.Clear();
                splitSpaces = namespacesCopy[k].Split('.');

                for (int j = 0; j < splitSpaces.Length; j++)
                {
                    if (j != 0)
                    {
                        _stringbuilder.Append('.');
                    }
                    _stringbuilder.Append(splitSpaces[j]);
                    if (!Namespaces.Contains(_stringbuilder.ToString()))
                    {
                        Namespaces.Add(_stringbuilder.ToString());
                    }
                }
            }
        }
    }
}
