﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private void AssembleBlockList()
        {
            DoBlock(RootBlock);
        }

        private void DoBlock(Block block)
        {
            if (block.StructureType != "member")
            {
                block.UnfoldDeclarations();
                block.PopulateVariables();
                BlockDictionary.Add(block.FileName, new Link(block.Link, block.MDLink));
                DocBlocks.Add(block);
            }

            if (block.ChildCount > 0)
            {
                for(int i = 0; i < block.Children.Count; i++)
                {
                    DoBlock(block.Children[i]);
                }
            }
        }
    }
}
