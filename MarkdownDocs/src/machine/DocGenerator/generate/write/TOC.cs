﻿using MarkdownDocs.model;
using MarkdownDocs.option;
using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        public void GenerateTOC()
        {
            TOC TableOfContents = new TOC(new TOC(RootBlock.GenerateTOC()));

            var serializer = new SerializerBuilder()
                .WithNamingConvention(new CamelCaseNamingConvention())
                .Build();
            string results = serializer.Serialize(TableOfContents);
            for (int i = 0; i < 6; i++)
            {
                results = results.Substring(results.IndexOf(Environment.NewLine)+1);
            }

            string tocName = "toc";
            string outDir = "";

            if (DocumentationOptions.Contains("global.TOC.name"))
            {
                tocName = DocumentationOptions.GetString("global.TOC.name");
            }
            if (DocumentationOptions.Contains("global.TOC.out"))
            {
                outDir = DocumentationOptions.GetString("global.TOC.out");
            } else if (DocumentationOptions.Contains("global.out.directory"))
            {
                outDir = DocumentationOptions.GetString("global.out.directory");
            }

            results = tocName + ":" + results;

            File.WriteAllText(outDir + "toc.yml", results);
        }
    }
}
