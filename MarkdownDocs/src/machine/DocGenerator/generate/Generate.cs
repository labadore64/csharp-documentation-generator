﻿
namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        public void Generate()
        {
            GenerateTOC();

            DocWriter writer = new DocWriter();

            for(int i = 0; i < DocBlocks.Count; i++)
            {
                writer.WritePage(DocBlocks[i]);
            }
        }
    }
}
