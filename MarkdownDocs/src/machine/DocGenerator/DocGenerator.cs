﻿using MarkdownDocs.option;
using System.IO;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        /// <summary>
        /// Generates the documents for source files in a certain directory.
        /// </summary>
        /// <param name="StartingDirectory">Root directory for source files</param>
        public void DocGenerate(string StartingDirectory, string FileExt)
        {
            LoadOptions();

            if (DocumentationOptions.GetString("global.writeDocs") != "false")
            {
                foreach (string file in Directory.GetFiles(
                    StartingDirectory, "*" + FileExt, SearchOption.AllDirectories))
                {
                    ParseFile(file);

                }

                Assemble();
                Generate();
            }
        }
    }
}
