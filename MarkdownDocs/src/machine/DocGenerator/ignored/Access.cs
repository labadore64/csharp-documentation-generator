﻿using System;
using System.Text;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private bool IsIgnoredAccess(string type)
        {
            for (int j = 0; j < IgnoredAccess.Length; j++)
            {
                if(IgnoredAccess[j] == type)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
