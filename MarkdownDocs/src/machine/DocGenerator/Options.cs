﻿using MarkdownDocs.model;
using MarkdownDocs.option;
using System;
using System.Collections.Generic;
using System.IO;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        string[] IgnoredAccess;

        private void LoadOptions()
        {
            IgnoredAccess = DocumentationOptions.GetStrings("global.ignore.access");
        }
    }
}
