﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        /// <summary>
        /// The root block for the object tree
        /// </summary>
        public static IndexBlock RootBlock;

        /// <summary>
        /// List of all blocks used for documentation.
        /// </summary>
        public static List<Block> DocBlocks = new List<Block>();

        /// <summary>
        /// Dictionary of links sorted by FullName
        /// </summary>
        public static Dictionary<string, Link> BlockDictionary = new Dictionary<string, Link>();

        public static List<string> Namespaces = new List<string>();

        public static Dictionary<string, TypeBlock> Blocks = new Dictionary<string, TypeBlock>();
    }
}
