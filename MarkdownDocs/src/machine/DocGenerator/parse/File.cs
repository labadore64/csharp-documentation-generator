﻿using MarkdownDocs.model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        int i = 0;

        private void AddLineOnRead(string s, List<string> stager, StringBuilder builder)
        {
            if (s.Length > 0)
            {
                stager.Add(s);
            }
            builder.Clear();
        }

        private void ParseFile(string Filename)
        {
            string[] _filetext = new string[0];

            string _namespace ="";
            string _type ="";

            Stack<Block> Stack = new Stack<Block>();

            List<string> stager = new List<string>();

            TypeBlock CurrentRoot;
            string text = "";
            if (File.Exists(Filename)){ 
                try
                {
                    text = File.ReadAllText(Filename).Replace(Environment.NewLine,"\n");

                    stager.Clear();
                    StringBuilder builder = new StringBuilder();

                    for (int j = 0; j < text.Length; j++)
                    {
                        if (text[j] == '\n' || text[j] == ';')
                        {
                            if (text[j] == ';')
                            {
                                builder.Append(';');
                            }
                            AddLineOnRead(builder.ToString().Trim(), stager, builder);
                        } else if (text[j] == '}' || text[j] == '{')
                        {
                            AddLineOnRead(builder.ToString().Trim(), stager, builder);
                            builder.Append(text[j]);
                            AddLineOnRead(builder.ToString().Trim(), stager, builder);
                        }
                        else
                        {
                            builder.Append(text[j]);
                        }
                    }

                    _filetext = stager.ToArray();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.ToString());
                    throw e;
                }

                CurrentRoot = new TypeBlock();
                Stack.Push(CurrentRoot);
                i = 0;
                string _currentString;
                while(i < _filetext.Length)
                {
                    _currentString = _filetext[i];

                    if (_currentString.Length > 0 &&
                        !(_currentString.StartsWith("//") && !_currentString.StartsWith("///")))
                    {
                        // line represents start of comment
                        if (_currentString.StartsWith("///"))
                        {
                            Declaration dec = ParseComment(_filetext,Stack,_type, CurrentRoot);

                            if (!IsIgnoredAccess(dec.AccessModifier))
                            {
                                Stack.Peek().AddDeclaration(dec);
                            }
                        } else if (_currentString.Contains('{'))
                        {
                            StartBlock(Stack,CurrentRoot);
                        }
                        else if (_currentString.Contains('}'))
                        {
                            EndBlock(Stack);
                        }
                        // is a using directive
                        else if (_currentString.StartsWith("using"))
                        {
                            CurrentRoot.UsingDirectives.Add(_currentString.Replace("using", "").Replace(";", "").Trim());
                        }
                        _currentString = _filetext[i];
                        // get namespace
                        if (Stack.Count == 1 && _currentString.StartsWith("namespace"))
                        {
                            _namespace = ParseNamespace(_filetext);
                        }

                        // get type
                        if(Stack.Count == 2 && 
                            (_currentString.Contains("class")
                             || _currentString.Contains("enum")
                             || _currentString.Contains("struct")
                             || _currentString.Contains("interface")
                            ))
                        {
                            _type=ParseType(_filetext);
                        }
                    }
                    i++;
                }
                CurrentRoot.Namespace = _namespace;
                CurrentRoot.TypeDeclaration = _type;

                string access = Declaration.GetAccessModifier(CurrentRoot.Namespace, CurrentRoot.TypeDeclaration);
                bool ignoreAccess = false;

                for(int i = 0; i < IgnoredAccess.Length; i++)
                {
                    if (IgnoredAccess[i].Contains(access))
                    {
                        ignoreAccess = true;
                    }
                }

                if ((CurrentRoot.DeclarationCount > 0
                    || CurrentRoot.ChildCount > 0)
                    && !ignoreAccess)
                {
                    if (Blocks.ContainsKey(CurrentRoot.FullName))
                    {
                        Blocks[CurrentRoot.FullName].Merge(CurrentRoot);
                    }
                    else
                    {
                        Blocks.Add(CurrentRoot.FullName, CurrentRoot);
                        if (!Namespaces.Contains(_namespace))
                        {
                            Namespaces.Add(_namespace);
                        }
                    }
                }
            }
        }
    }
}
