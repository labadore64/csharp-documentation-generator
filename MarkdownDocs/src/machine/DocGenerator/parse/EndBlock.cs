﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private Block EndBlock(Stack<Block> Stack)
        {
            Block child = Stack.Pop();

            if(child.ChildCount == 0 && child.DeclarationCount == 0)
            {
                // remove this child from the parent
                if (child.Parent != null)
                {
                    child.Parent.RemoveChild(child);
                }
            } else if(child.DeclarationCount == 0)
            {
                Block parent = child.Parent;
                if (parent != null)
                {
                    // remove this child from the parent
                    parent.RemoveChild(child);

                    // add this block's children to the parent
                    for (int i = 0; i < child.Children.Count; i++)
                    {
                        parent.AddChild(child.Children[i]);
                    }
                    child.ClearChildren();
                }
            }
            else if (child.ChildCount == 0)
            {
                Block parent = child.Parent;
                // remove this child from the parent
                if (parent != null)
                {
                    parent.RemoveChild(child);

                    // add this block's children to the parent
                    for (int i = 0; i < child.Declarations.Count; i++)
                    {
                        parent.AddDeclaration(child.Declarations[i]);
                    }
                    child.ClearDeclarations();
                }
            }

            return child;
        }
    }
}
