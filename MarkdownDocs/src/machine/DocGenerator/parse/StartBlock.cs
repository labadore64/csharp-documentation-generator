﻿using MarkdownDocs.model;
using System.Collections.Generic;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        private Block StartBlock(Stack<Block> Stack, Block CurrentRoot)
        {
            Block block = new Block();
            Block parent = CurrentRoot;
            if (Stack.Count > 0)
            {
                parent = Stack.Peek();
            }

            block.Parent = parent;
            parent.AddChild(block);
            
            Stack.Push(block);
            return block;
        }
    }
}
