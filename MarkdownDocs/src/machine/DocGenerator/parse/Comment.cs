﻿using MarkdownDocs.model;
using System.Collections.Generic;
using System.Text;

namespace MarkdownDocs.machine
{
    internal partial class DocGenerator
    {
        StringBuilder _stringbuilder = new StringBuilder();
        int StackPosition;

        private Declaration ParseComment(string[] _filetext, Stack<Block> Stack, string _type, Block CurrentRoot)
        {
            string comment;
            string dec;

            // first get the comment itself
            _stringbuilder.Clear();

            while (i < _filetext.Length)
            {
                _filetext[i] = _filetext[i].Trim();

                // line represents start of comment
                if (_filetext[i].StartsWith("///"))
                {

                    _stringbuilder.Append(_filetext[i].Remove(0,3));
                }
                else
                {
                    break;
                }
                i++;
            }
            comment = _stringbuilder.ToString();

            int currentStack = Stack.Count;

            _stringbuilder.Clear();

            // now get the statement attached to the comment
            while (i < _filetext.Length)
            {
                _filetext[i] = _filetext[i].Trim();

                // line represents start or end of block
                if (_filetext[i].Contains('{') || _filetext[i].Contains('}'))
                {
                    if (i + 1 < _filetext.Length && _filetext[i + 1].Contains("get;"))
                    {
                        StackPosition = Stack.Count;
                        DoGetSet(i, _filetext, Stack, CurrentRoot);
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                // line represents end of statement
                else if (_filetext[i].EndsWith(';'))
                {
                    _stringbuilder.Append(_filetext[i] + " ");
                    break;
                }
                else if (_filetext[i].StartsWith("///"))
                {
                    break;
                }
                else
                {
                    _stringbuilder.Append(_filetext[i] + " ");
                }
                i++;
            }

            // if sitting on a { or } you need to push it back one
            // this lets you read the statement properly
            if(_filetext[i] == "{" || _filetext[i] == "}")
            {
                i--;
            }

            dec = _stringbuilder.ToString();

            return new Declaration(comment, dec, _type);
        }

        private void DoGetSet(int i, string[] _filetext, Stack<Block> Stack, Block CurrentRoot)
        {
            while (i < _filetext.Length && StackPosition <= Stack.Count)
            {
                _stringbuilder.Append(_filetext[i] + ' ');
                if (_filetext[i].Contains('{'))
                {
                    StartBlock(Stack, CurrentRoot);
                }
                else if (_filetext[i].Contains('}'))
                {
                    EndBlock(Stack);
                    break;
                }
                i++;
            }
        }
    }
}
