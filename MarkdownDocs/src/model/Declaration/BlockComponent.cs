﻿using MarkdownDocs.src.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace MarkdownDocs.model
{
    internal partial class BlockComponent
    {
        public Dictionary<string, string> Comments = new Dictionary<string, string>();
        public string Statement;
        public string AccessModifier;
        public string Type;
        public string TypeName;
        public string[] Params = new string[0];
        public string GenericType = "";

        public BlockComponent(Declaration dec)
        {
            Statement = dec.Statement;
            AccessModifier = dec.AccessModifier;
            Type = dec.Type;

            if (Statement.Contains("(") && Type != "property")
            {
                Params = Statement.Replace("(", "{{")
                            .Replace(")", "}}")
                            .EverythingBetween("{{", "}}")
                            .ToArray();
                if (Params.Length > 0)
                {
                    Params = Params[0].Split(", ");
                }
            }
            TypeName = dec.TypeName;

            GenerateComments("<root>"+dec.Comments+"</root>");
            
        }

        private void GenerateComments(string xml)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            XmlElement xnList = xmlDoc["root"];
            int increment = 1;

            foreach(XmlElement e in xnList.ChildNodes)
            {
                increment = 1;

                if (e.Name == "param")
                {
                    e.InnerText += ","+e.Attributes[0].Value;
                }

                if (!Comments.ContainsKey(e.Name))
                {
                    Comments.Add(e.Name, e.InnerText.Trim());
                } else
                {
                    while (Comments.ContainsKey(e.Name + increment))
                    {
                        increment++;
                    }
                    Comments.Add(e.Name + increment, e.InnerText.Trim());
                }
            }

            Dictionary<string, string> newVars = new Dictionary<string, string>();

            int icounter = 0;

            foreach (KeyValuePair<string, string> s in Comments)
            {
                if (s.Key.Contains("param") && !s.Key.Contains("typeparam"))
                {
                    string[] paramParts = s.Value.Split(",");
                    newVars.Add("paramtype"+ icounter, "");
                    newVars.Add("paramname" + icounter, "");
                    newVars.Add("paramdesc" + icounter, "");

                    if (paramParts.Length > 1)
                    {
                        newVars["paramname" + icounter] = paramParts[1];
                    }
                    if (paramParts.Length > 0)
                    {
                        newVars["paramdesc" + icounter] = paramParts[0];
                    }

                    string paramname = newVars["paramname" + icounter];

                    for (int i = 0; i < Params.Length; i++)
                    {
                        if (Params[i].Contains(paramname))
                        {
                            newVars["paramtype" + icounter] = Params[i].Substring(0, Params[i].IndexOf(" "));
                        }
                    }

                    icounter++;
                } else if (s.Key.Contains("returns"))
                {
                    string typer = Statement.
                                    Substring(Statement.IndexOf(AccessModifier) + AccessModifier.Length);

                    newVars.Add("returntype",typer.Substring(0,typer.IndexOf(TypeName)).Replace("static",""));
                }
            }

            foreach(KeyValuePair<string,string> s in newVars){
                Comments.Add(s.Key, s.Value);
            }
        }
    }
}
