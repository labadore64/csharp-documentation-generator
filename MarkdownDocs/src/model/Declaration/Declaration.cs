﻿using System;
using System.Linq;

namespace MarkdownDocs.model
{
    internal partial struct Declaration
    {
        public string Comments;
        public string Statement;
        public string AccessModifier;
        public string Type;
        public string TypeName;

        public Declaration(string Comments, string Statement, string rootType)
        {
            Comments = Comments.Trim();
            Statement = Statement.Trim();
            this.Comments = Comments;
            this.Statement = Statement;

            Type = "field";
            string[] parts;
            
            if(Statement.Contains("{")){
                Statement = Statement.Substring(0, Statement.LastIndexOf("{")).Trim();
            }

            if (Statement.Contains("("))
            {
                Statement = Statement.Substring(0, Statement.LastIndexOf("(")).Trim();
            }

            parts = Statement.Split(' ');

            TypeName = parts[parts.Length - 1];
            AccessModifier = GetAccessModifier(rootType,this.Statement);
            GetTheType(rootType);
        }

        public static string GetAccessModifier(string rootType, string statement)
        {
            string accessModifier = "private";
            if (rootType.Contains("interface") ||
                rootType.Contains("enum"))
            {
                accessModifier = "public";
            }
            else
            {
                if (statement.Contains("public"))
                {
                    accessModifier = "public";
                }
                else if (statement.Contains("protected"))
                {
                    accessModifier = "protected";
                }
                else if (statement.Contains("internal"))
                {
                    accessModifier = "internal";
                }
            }

            return accessModifier;
        }

        private void GetTheType(string rootType)
        {
            if (Statement.Contains("get;") ||
                Statement.Contains("set;"))
            {
                Type = "property";
            }
            else if (Statement.Contains("class"))
            {
                Type = "class";
            }
            else if (Statement.Contains("enum"))
            {
                Type = "enum";
            }
            else if (Statement.Contains("interface"))
            {
                Type = "interface";
            }
            else if (Statement.Contains("struct"))
            {
                Type = "struct";
            }
            else if (Statement.Contains("const"))
            {
                Type = "constant";
            }
            else if (Statement.Contains('(') &&
                Statement.Contains(')'))
            {
                Type = "method";

                if (!rootType.Contains("interface")){
                    if (Statement.Substring(0, Statement.IndexOf('(')).Split().Length == 2)
                    {
                        Type = "constructor";
                    } else if (Statement.Contains("operator"))
                    {
                        Type = "operator";
                    }
                    else if (Statement.Contains("~"))
                    {
                        Type = "deconstructor";
                    }
                }
            }
        }

        public override string ToString()
        {
            return Comments + Environment.NewLine + 
                Statement + Environment.NewLine +
                "Access: " + AccessModifier + Environment.NewLine +
                "Type: " + Type + Environment.NewLine;
        }
    }
}
