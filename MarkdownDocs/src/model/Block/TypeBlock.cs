﻿namespace MarkdownDocs.model
{
    internal class TypeBlock : Block
    {
        public override bool CreatePage { get { return true; } }
        public override string TypeDeclaration
        { get
            {
                return _typeDec;
            }
            set
            {
                _typeDec = value;
                _structureType = "";
                if(_typeDec.Contains(" enum "))
                {
                    _structureType = "enum";
                } else if (_typeDec.Contains(" struct "))
                {
                    _structureType = "struct";
                } else if (_typeDec.Contains(" interface "))
                {
                    _structureType = "interface";
                }
                else if (_typeDec.Contains(" class "))
                {
                    _structureType = "class";
                }

                if (StructureType.Length != 0)
                {
                    string[] typestrings = _typeDec.Substring(_typeDec.LastIndexOf(StructureType) + StructureType.Length).Split(':');
                    TypeName = typestrings[0].Trim();

                    FullName = Namespace.Trim() + "." + TypeName.Trim();
                    if (typestrings.Length > 1)
                    {
                        Implement = typestrings[1].Split(',');
                    } else
                    {
                        Implement = new string[0];
                    }
                }
            }
        }
        string _typeDec;

        public override string StructureType { get { return _structureType; } }
        string _structureType;
        public void Merge(Block other)
        {
            for(int j = 0; j < other.Declarations.Count; j++)
            {
                AddDeclaration(other.Declarations[j]);
            }
            for (int j = 0; j < other.Children.Count; j++)
            {
                AddChild(other.Children[j]);
            }

            for(int j = 0; j < other.UsingDirectives.Count; j++)
            {
                if (!UsingDirectives.Contains(other.UsingDirectives[j]))
                {
                    UsingDirectives.Add(other.UsingDirectives[j]);
                }
            }
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}
