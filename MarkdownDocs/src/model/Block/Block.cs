﻿using MarkdownDocs.option;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MarkdownDocs.model
{
    internal class Block
    {
        static string BaseURL = DocumentationOptions.GetString("docitem.baseURL");

        public virtual string FileName 
        { 
            get
            {
                return FullName
                        .Replace("<", "")
                        .Replace(">", "");
            } 
        }

        public string MDLink 
        { 
            get => 
                "[" + TypeName
                        .Replace("<", " < ")
                        .Replace(">", " > ")
                + "]" + "(" + Link + ")"; 
        }

        public string Link
        {
            get =>
                BaseURL + FileName;
        }

        public virtual bool CreatePage { get { return false; } }
        public string[] Implement { get; protected set; }
        public virtual string Namespace { get; set; }
        public List<Declaration> Declarations { get; private set; } = new List<Declaration>();
        public List<BlockComponent> Components { get; private set; } = new List<BlockComponent>();
        public List<Block> Children { get; private set; } = new List<Block>();
        public List<string> UsingDirectives { get; private set; } = new List<string>();
        public Block Parent { get; set; }
        public virtual string TypeName { get; protected set; }

        public virtual string TypeDeclaration { get; set; }

        public Dictionary<string, string> Variables { get; protected set; } = new Dictionary<string, string>();

        public TypeBlock Root { get; set; }

        public int ChildCount { get { return Children.Count; } }

        public int DeclarationCount { get { return Declarations.Count; } }

        public virtual string StructureType { get { return "member"; } }
        public virtual string FullName { get; protected set; }
        public void AddDeclaration(Declaration declaration)
        {
            // if it starts with < it means its valid xml
            if (declaration.Comments.StartsWith('<'))
            {
                Declarations.Add(declaration);
            }
        }

        public void RemoveDeclaration(Declaration declaration)
        {
            Declarations.Remove(declaration);
        }

        public void ClearDeclarations()
        {
            Declarations.Clear();
        }

        public void AddChild(Block block)
        {
            Children.Add(block);
            block.Parent = this;
        }

        public void RemoveChild(Block block)
        {
            Children.Remove(block);
            block.Parent = null;
        }

        public void ClearChildren()
        {
            Children.Clear();
        }

        public override string ToString()
        {
            return FullName;
        }

        public void UnfoldDeclarations()
        {
            for(int i = 0; i < Declarations.Count; i++)
            {
                Components.Add(new BlockComponent(Declarations[i]));
            }
        }

        public TOC GenerateTOC()
        {
            if (CreatePage)
            {
                TOC returner = new TOC();
                if (GetType() == typeof(TypeBlock))
                {
                    returner.Title = TypeName;
                }
                else
                {
                    returner.Title = ToString();
                }
                returner.url = Link;
                TOC holder;
                for(int i = 0; i < Children.Count; i++)
                {
                    holder = Children[i].GenerateTOC();
                    if(holder != null)
                    {
                        returner.Children.Add(holder);
                    }
                }

                return returner;
            } else
            {
                return null;
            }
        }

        public virtual void WriteFile(string Directory, string Contents)
        {
            File.WriteAllText(Directory + FileName + ".md", Contents);
        }

        public string GetVar(string key)
        {

            if (Variables.ContainsKey(key))
            {
                return Variables[key];
            }

            return "";
        }

        public void PopulateVariables()
        {
            Variables.Clear();

            Variables.Add("typename", TypeName);
            Variables.Add("name", TypeName);
            Variables.Add("structureType", StructureType);

            Variables.Add("declaration", TypeDeclaration);

            Variables.Add("namespace", Namespace);

            BlockComponent summary = Components.Find(
                    x =>
                        x.Type == "class" ||
                        x.Type == "enum" ||
                        x.Type == "interface" ||
                        x.Type == "struct"
                    );

            if(summary != null)
            {
                Variables.Add("summary", summary.Comments["summary"]);
            }
        }
    }
}
