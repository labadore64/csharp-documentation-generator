﻿using MarkdownDocs.option;

namespace MarkdownDocs.model
{
    internal class IndexBlock : Block
    {
        public override string FullName { get => StructureType; }
        public override string TypeName { get => StructureType; }
        public override bool CreatePage { get { return true; } }
        public override string StructureType { get { return "index"; } }

        public override string Namespace { get => ""; }
        public string IndexContent { get; set; }

        public override string ToString()
        {
            return DocumentationOptions.GetString("global.indexName");
        }
    }
}
