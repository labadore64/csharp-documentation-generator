﻿namespace MarkdownDocs.model
{
    internal class NamespaceBlock : Block
    {
        public override string FullName { get => Namespace; }
        public override string TypeName { get => Namespace; }
        public override bool CreatePage { get { return true; } }

        public override string StructureType { get { return "namespace"; } }
        public bool CompareNamespace(string Namespace)
        {
            if(Namespace == this.Namespace)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return Namespace;
        }
    }
}
