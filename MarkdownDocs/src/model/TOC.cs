﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MarkdownDocs.model
{
    internal class TOC
    {

        public string Title { get; set; }

        public string url { get; set; }
        public List<TOC> Children { get; set; } = new List<TOC>();

        public override string ToString()
        {
            return Title;
        }

        public TOC()
        {

        }

        public TOC(TOC toc)
        {
            Children.Add(toc);
        }
    }
}
