﻿using System;
using System.Linq;

namespace MarkdownDocs.model
{
    internal partial struct Link
    {
        public string URLLink;
        public string MDLink;

        public Link(string URLLink, string MDLink)
        {
            this.URLLink = URLLink;
            this.MDLink = MDLink;
        }
    }
}
